package com.jt.demo.controller;

import com.jt.demo.mapper.UserMapper;
import com.jt.demo.pojo.User;
import com.jt.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Controller
public class UserController {
    @Autowired
    private UserService userService;
    @RequestMapping("/findAll")
    @ResponseBody
    public List<User> findAll(){
       return userService.findAll();
    }
    /*
    * 实现页面跳转
    * url： http：//localhost：8090/userList
    * 页面取值 ${userList}
    *     * */
    @RequestMapping("/userList")
   // @ResponseBody//1 将返回值结果转化成json 2表示程序结束 3 不会执行视图解析器的配置
    public String userList(Model model){
        List<User> userList=userService.findAll();
        //利用model将数据寸到request域
        model.addAttribute("userList",userList);
        return "userList";
    }
    /*
    跳转到ajax页面
    */
    @RequestMapping("ajaxuser")
    public String toagax(){
        return "ajaxuserList";
    }
}
