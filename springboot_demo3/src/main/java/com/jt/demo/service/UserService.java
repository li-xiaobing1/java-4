package com.jt.demo.service;

import com.jt.demo.pojo.User;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface UserService {
    public List<User> findAll();
}
