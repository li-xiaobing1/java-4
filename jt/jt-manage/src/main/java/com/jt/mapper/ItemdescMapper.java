package com.jt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.ItemDesc;

public interface ItemdescMapper extends BaseMapper<ItemDesc> {
}
