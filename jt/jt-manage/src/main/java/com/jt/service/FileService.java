package com.jt.service;

import com.jt.vo.ImageVo;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public interface FileService {
    ImageVo upload(MultipartFile uploadFile);
}
