package com.jt.service;

import com.google.common.reflect.TypeToken;
import com.jt.vo.ImageVo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import javax.swing.plaf.nimbus.State;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Service
@PropertySource(value = "classpath:/properties/image.properties",encoding = "UTF-8")
public class FileServiceimpl implements FileService{
    //方法1 利用集合实现用户校验
    //方法2 利用正则表达式
    private  static  Set<String> typeSet=new HashSet<>();
    @Value("${image.localDir}")
    private String localDir;     //="E:/java4";
    @Value("${image.urlPath}")
    private String UrlPath;     //  ="http://image.jt.com";
    static {
        typeSet.add(".jpg");
        typeSet.add(".png");
        typeSet.add(".gif");
    }
    /**
     * 文件上传业务说明:
     * 1.校验文件上传是否为图片类型? jpg|png|gif....
     * 2.为了防止恶意程序 木马.exe.jpg
     * 3.为了保证检索速度,需要将图片 分目录存储.
     * 4.如何防止文件重名???
     * @param uploadFile
     * @return
     *  * 正则语法:
     *      *  1.{n} 指定次数
     *      *  2.[a-z] 字符必须满足集合中的一个元素
     *      *  3.(a|n|z) 分组的写法  满足a或者n或者z
     *
     */
    @Override
    public ImageVo upload(MultipartFile uploadFile) {
        //1.1校验文件类型
        String fileName= uploadFile.getOriginalFilename().toLowerCase();
        //1.2 利用正则表达式匹配数据类型
        if (fileName.matches("^.+\\.(png|gif|jpg)$"))
            try {
                BufferedImage bufferedImage = ImageIO.read(uploadFile.getInputStream()) ;
                int width=bufferedImage.getWidth();
                int height=bufferedImage.getHeight();
                if (width==0||height==0) {
                    return ImageVo.fail();
                    //实现分目录存储
                }
                    String localDirPath =
                            new SimpleDateFormat("/yyyy/MM/dd/").format(new Date());
                    String fileDir = localDir+localDirPath;
                    File filePath = new File(fileDir);
                    if (!filePath.exists()){//判断文件目录是否存在

                        filePath.mkdirs();  //创建目录

                    }
                    //4 利用uuid动态生成图片名称       去除生成的“-”
                String uuid =UUID.randomUUID().toString().replace("-", "");
                String fileType=fileName.substring(fileName.lastIndexOf("."));   //substring()截取
                String newFileName= uuid+fileType;
                //5实现文件上传
                File realFile = new File(localDir+newFileName);
                uploadFile.transferTo(realFile);//文件上传api
                //6编辑图片虚拟路径     "协议：//域名地址：端口号/图片存储路径/图片真实名称"
                //6.1磁盘地址: E:\JT_IMAGE\2021\01\26\1b0e435933ac42cabec53b20ffbcfe90.png
                //6.2虚拟地址  http://image.jt.com\2021\01\26\1b0e435933ac42cabec53b20ffbcfe90.png
                String url=UrlPath+localDirPath+realFile;
                return ImageVo.success(url,null ,null );
            }catch (IOException e){
                e.printStackTrace();
                return ImageVo.fail();
            }
        return null;
    }
    /**
     * 文件上传业务说明:
     * 1.校验文件上传是否为图片类型? jpg|png|gif....
     * 2.为了防止恶意程序 木马.exe.jpg
     * 3.为了保证检索速度,需要将图片 分目录存储.
     * 4.如何防止文件重名???
     * @param uploadFile
     * @return
     */
   /* @Override
    public ImageVo upload(MultipartFile uploadFile) {
        //校验是否为图片类型
        //1.1 获取文件的类型   XX.jpg
        String fileName=uploadFile.getOriginalFilename();
        //为防止由于大小写造成的校验异常
        fileName=fileName.toLowerCase();//转成小写
        //1.2 获取文件类型
      *//*关于bug 没有后缀 *//* int index=fileName.lastIndexOf(".");
        if(index==-1){
            //如果下标为-1 程序无后缀，应提前结束
            return ImageVo.fail();
        }
        String fileType=fileName.substring(fileName.lastIndexOf("."));
        if (!typeSet.contains(fileType)){
            //图片类型不符
            return ImageVo.fail();
        }

        return null;
    }*/
}
