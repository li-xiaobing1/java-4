package com.jt.aop;

import com.jt.vo.SysResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

//全局异常处理机制
@RestControllerAdvice
public class SystemAop {
    @ExceptionHandler({RuntimeException.class})
    // @ExceptionHandler   异常处理器  RuntimeException.class 运行期异常
    public Object result(Exception e){
        e.printStackTrace();
        return SysResult.fail();
    }
}
