package com.jt.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@TableName("tb_item_cat")
@Data
@Accessors(chain = true)
public class ItemCat extends BasePojo implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer id;    //商品分类id
    private Integer parentId; //定义父及分类id
    private String name; //商品分类名称
    private Integer status; //指定状态 1 正常 2 删除
    private Integer sortOrder; //排序号
    private  Boolean isParent; //是否为父级
}
