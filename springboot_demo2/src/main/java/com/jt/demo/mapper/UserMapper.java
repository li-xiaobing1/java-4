package com.jt.demo.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.demo.pojo.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
//继承接口时  切记添加泛型
public interface UserMapper extends BaseMapper<User> {
	
	List<User> findAll();
}
