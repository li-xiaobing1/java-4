package com.jt.demo.test;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.injector.methods.SelectObjs;
import org.apache.ibatis.javassist.runtime.Desc;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.jt.demo.mapper.UserMapper;
import com.jt.demo.pojo.User;

import static org.assertj.core.api.BDDAssertions.and;

@RunWith(SpringRunner.class)	//注意测试文件的位置 必须在主文件加载包路径下
@SpringBootTest
public class TestMybatis {
	
	@Autowired
	private UserMapper userMapper;
	
	@Test
	public void testFindUser() {
		
		List<User> userList = userMapper.findAll();
		System.out.println(userList);
	}
	/*完成用户信息入库*/
	@Test
	public void testInsert(){
		User user = new User();
		user.setName("郑爽").setAge(30).setSex("女");
		//单表的sql语句几乎不写
		userMapper.insert(user);
	}
	/*
	查询案例1：id=4的用户
	id代表主键
	*/
	@Test
	public void testSelectById(){
		User user=userMapper.selectById(4);
		System.out.println(user);
	}
	/*
	查询案例2：查询name=八戒的人
	QueryWarpper<> 条件构造器用来拼接where条件
	  * QueryWrapper<> 条件构造器 用来拼接where条件
     * 常见逻辑运算符:   = eq, > gt , < lt
     *                  >= ge, <= le
	*/
	@Test
	public void testSelectByName(){
		/*User user = new User();
		user.setName("八戒");
		//根据对象中不为null的属性拼接where条件
		QueryWrapper<User> queryWrapper=new QueryWrapper<>(user);

		List<User> userList=userMapper.selectList(queryWrapper);
		System.out.println(userList);*/
		QueryWrapper<User> queryWrapper = new QueryWrapper<>() ;
		queryWrapper.eq("name", "小乔");
		System.out.println(userMapper.selectList(queryWrapper));
	}
	/*查询案例3：age》18 且名字含“精”用户
	* 如果中间是and连接符可省略不写
	* */
	@Test
	public void selectAndd(){
		QueryWrapper<User> queryWrapper=new QueryWrapper<>();
		queryWrapper.gt("age", 18).like("name","精");
		System.out.println(userMapper.selectList(queryWrapper));
	}
/*
	查询name不为null且sex=女，name要求以君结尾
	最后将我们的数据按id倒序排列
	sql: select * from user
*/
	@Test
	public void selectAnd2(){
		QueryWrapper<User> queryWrapper= new QueryWrapper<>();
		queryWrapper.isNotNull("name").eq("sex","女").
				likeLeft("name", "君").orderByDesc("id");
		System.out.println(userMapper.selectList(queryWrapper));
	}
/*
	批量查询 查询id=1，2,3,4,5,7,8的数据
	关键字: in 包含
*/@Test
	public void selectIn(){
		QueryWrapper queryWrapper= new QueryWrapper();
		queryWrapper.in("id", 1,2,3,5,7);//不能输入数组，数组是哥基本结构没有取值方法，所以要通过集合
		System.out.println(userMapper.selectList(queryWrapper));
	}
	@Test
	public void selectIn1(){
	//数组是哥基本结构没有取值方法，所以要通过集合
	Integer[] idArray={1,2,4,6,8};
	//数组转化是使用包装类型
		List<Integer> idList= Arrays.asList(idArray);
		QueryWrapper queryWrapper= new QueryWrapper();
		queryWrapper.in("id", idList);//不能输入数组，数组是哥基本结构没有取值方法，所以要通过集合
		System.out.println(userMapper.selectList(queryWrapper));
		//该方法与上操作一致，写法不同
		userMapper.selectBatchIds(idList);
		//SelectObjs获取数据表中第一个字段信息（主键）
		System.out.println(userMapper.selectObjs(null));
	}
	/*
	更新
	1.更新id=71的数据name=张翰
	2。将name=郑爽 改为name=胡彦斌 sex=男 age=20
	userMapper.update(修改后的是数据对象set条件，修改后的条件构造器where条件)

	*/
	public void testupdate(){
		User user=new User();
		//id当做where条件，其他不为空的属性当做set条件
		user.setId(71).setName("张翰");
		userMapper.updateById(user);
	}
	public void testupdate2(){
		User user=new User();
		//id当做where条件，其他不为空的属性当做set条件
		user.setName("胡彦斌").setSex("男").setAge(20);
		UpdateWrapper updateWrapper=new UpdateWrapper();
		updateWrapper.eq("name","郑爽");
		userMapper.update(user,updateWrapper);
	}
}
