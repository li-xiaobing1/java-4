package com.jt.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@PropertySource("classpath:properties/user.properties")
public class UserController {
        //spel表达式
        //      该注解只能获得spring容器的数据，前提条件就是改数据必须被容器加载
        @Value("${redis.user}")
        private String host;
        @Value("${redis.port}")
        private Integer port;
        @RequestMapping("/getNode")
        public String getNode(){
                return host+":"+port;
        }
}
