package com.jt.pojo;


import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Component;

@Data //动态添加get、settostring equal hash 方法
@Accessors(chain=true)//链式加载
@Component
public class User {
    private Integer id;
    private String name;
    private Integer age;
    private String sex;
    //链式加载原理   重写set方法
/*
  public User SetId(Integer id){
        this.id=id;
        return this;
    }
    @Accessors(chain=true)可帮我们依次编写
    ......
    */
}
